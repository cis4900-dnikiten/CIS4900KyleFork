// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once 
#include "GameFramework/HUD.h"
#include "CIS4900HUD.generated.h"

UCLASS()
class ACIS4900HUD : public AHUD
{
	GENERATED_BODY()

public:
	ACIS4900HUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

