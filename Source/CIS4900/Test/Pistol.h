// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Test/BaseWeapon.h"
#include "Pistol.generated.h"

/**
 * 
 */
UCLASS()
class CIS4900_API APistol : public ABaseWeapon
{
	GENERATED_BODY()
	
public:
	APistol();
	
	virtual void SuccesfulFire() override;
	
};
