// Fill out your copyright notice in the Description page of Project Settings.

#include "CIS4900.h"
#include "GameplayPlayerController.h"
#include "GameplayHUD.h"
#include "GameplayCharacter.h"
#include "PlayerStats.h"

AGameplayPlayerController::AGameplayPlayerController()
{
	
}

void AGameplayPlayerController::TellHUDDamage(float HealthDamage, float ArmorDamage)
{
	CurrentHUD->OnTakeDamage(ArmorDamage, HealthDamage);
}
void AGameplayPlayerController::TellHUDDead()
{
	CurrentHUD->OnDead();
}

void AGameplayPlayerController::BeginPlay()
{
	CurrentHUD = Cast<AGameplayHUD>(GetHUD());
	Character = Cast<AGameplayCharacter>(GetPawn());
}

void AGameplayPlayerController::PlayerTick(float Delta)
{
	Super::PlayerTick(Delta);
	
	float Sensitivity = CVarSensitivity.GetValueOnGameThread();
	if (Sensitivity != MouseSensitivityCurrent)
	{
		this->SetSensitivity(Sensitivity);
	}

	UpdateHUD();
}

void AGameplayPlayerController::UpdateHUD()
{
	if (CurrentHUD && Character)
	{
		CurrentHUD->UpdateStats(Character->GetStats());
	}
}

float AGameplayPlayerController::GetSensitvity()
{
	return MouseSensitivityCurrent;
}

void AGameplayPlayerController::SetSensitivity(const float NewSensitivity)
{
	MouseSensitivityCurrent = FMath::Clamp(NewSensitivity, MouseSensitivityMin, MouseSensitivityMax);
}

void AGameplayPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	//setup input
	InputComponent->BindAxis("LookUp", this, &AGameplayPlayerController::MouseYAxis);
	InputComponent->BindAxis("Turn", this, &AGameplayPlayerController::MouseXAxis);
}

void AGameplayPlayerController::MouseYAxis(float AxisValue)
{
	AddPitchInput(AxisValue * MouseSensitivityCurrent);
}
void AGameplayPlayerController::MouseXAxis(float AxisValue)
{
	AddYawInput(AxisValue * MouseSensitivityCurrent);
}
