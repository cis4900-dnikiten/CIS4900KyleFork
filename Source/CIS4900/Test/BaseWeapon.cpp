// Fill out your copyright notice in the Description page of Project Settings.

#include "CIS4900.h"
#include "GameplayCharacter.h"
#include "BaseWeapon.h"
#include "AIController.h"


// Sets default values
ABaseWeapon::ABaseWeapon() :DamagePerShot(5.0f), CurrentAmmoInMag(10), MaxAmmoInMag(10), CurrentAmmoInInventory(0), MaxAmmoInInventory(100), FireRate(1.0f), LineTraceDistance(4000), bAutomatic(true),
ReloadTime(1.0f), InaccuracyAngle(0)
{
	bInfiniteMagAmmo = false;
	bInfiniteInventoryAmmo = false;
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("root"));
	this->SetRootComponent(Root);

	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMesh"));
	WeaponMesh->SetCollisionResponseToChannel(WEAPON_TRACE_CHANNEL, ECR_Ignore);
	WeaponMesh->SetCastShadow(false);
	WeaponMesh->AttachTo(GetRootComponent());

	EndDelegate.BindUObject(this, &ABaseWeapon::EndAnimation);

	AttachSocketName_Muzzle = FName("Muzzle");

	AmmoOnPickup = MaxAmmoInInventory / 2;
}

ECollisionChannel ABaseWeapon::GetWeaponTraceChannel()
{
	APawn* Player = Cast<APawn>(GetOwner());
	if (!Player) return ECC_Camera;

	AController* PlayerController = Cast<APlayerController>(Player->GetController());
	if (PlayerController) return WEAPON_TRACE_CHANNEL;
	else return BADMAN_TRACE_CHANNEL;
}

// Called when the game starts or when spawned
void ABaseWeapon::BeginPlay()
{
	Super::BeginPlay();

	CurrentAmmoInMag = MaxAmmoInMag;

	TraceParams = FCollisionQueryParams(FName(TEXT("WeaponTrace")), true, this);
	
	/*const FName TraceTag = FName("WeaponTraceTag");
	GetWorld()->DebugDrawTraceTag = TraceTag;
	TraceParams.TraceTag = TraceTag;*/
	
	TraceParams.bTraceAsyncScene = true;
	TraceParams.bReturnPhysicalMaterial = true;

	APawn* Owner = Cast<APawn>(GetOwner());
	if (Owner)
		OwningCharacter = Cast<AGameplayCharacter>(Owner);
}

// Called every frame
void ABaseWeapon::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
}

void ABaseWeapon::StartFire()
{
	//GetWorldTimerManager().SetTimer(FireHandle, this, &ABaseWeapon::Fire, FMath::Max(FireRate,0.1f), bAutomatic, 0.0f);
	bWantsToFire = true;
	Fire();
}
void ABaseWeapon::StopFire()
{
	bWantsToFire = false;
	//GetWorldTimerManager().ClearTimer(FireHandle);
}

void ABaseWeapon::Fire()
{
	if (CurrentAmmoInMag == 0 && !bInfiniteMagAmmo)
	{
		FailedFire();
	}
	else
	{
		if(CurrentState == EGunStateEnum::Type::GSE_Idle) SuccesfulFire();
	}
}

bool ABaseWeapon::Trace(const FVector & StartLoc, const FRotator & StartRot, FHitResult& Result)
{
	const FVector EndLoc = StartLoc + StartRot.Vector() * LineTraceDistance;

	TArray<FHitResult> Results = UCIS4900Statics::ConeTraceByChannel(GetWorld(), StartLoc, StartRot, LineTraceDistance, 1, InaccuracyAngle, GetWeaponTraceChannel(), TraceParams);
	Result = Results[0];

	if (!Result.bBlockingHit) Result.Location = EndLoc;
	return Result.bBlockingHit;
}

bool ABaseWeapon::CanDamage(const AActor* Other)
{
	return (IsValid(Other) && Other->bCanBeDamaged);
}

bool ABaseWeapon::Reload()
{

	if ((bInfiniteInventoryAmmo || (IsMagNotFull() && HaveAmmo()) ) && CurrentState == EGunStateEnum::Type::GSE_Idle)
	{
		CurrentState = EGunStateEnum::Type::GSE_Reloading;
		if (WeaponMesh && ReloadMontage && WeaponMesh->GetAnimInstance())
		{

			float TimeTillCompletion = WeaponMesh->GetAnimInstance()->Montage_Play(ReloadMontage);
			WeaponMesh->GetAnimInstance()->Montage_SetPlayRate(ReloadMontage, GetReloadRate(TimeTillCompletion));
			//Skip the pulling the trigger part of the animation
			WeaponMesh->GetAnimInstance()->Montage_JumpToSection(FName("ReloadStart"), ReloadMontage);
			WeaponMesh->GetAnimInstance()->Montage_SetEndDelegate(EndDelegate);
		}

		return true;
	}

	return false;
}

void ABaseWeapon::FailedFire()
{
	if (CurrentAmmoInInventory == 0 && !bInfiniteInventoryAmmo && CurrentState == EGunStateEnum::Type::GSE_Idle)
	{
		PlaySound(EmptySound);
	}
	else
	{
		Reload();
	}
}
void ABaseWeapon::SuccesfulFire()
{
	CurrentState = EGunStateEnum::Type::GSE_Firing;
	if (WeaponMesh && FireMontage && WeaponMesh->GetAnimInstance())
	{
		float TimeTillCompletion = WeaponMesh->GetAnimInstance()->Montage_Play(FireMontage);
		WeaponMesh->GetAnimInstance()->Montage_SetPlayRate(FireMontage, GetFireRate(TimeTillCompletion));
		WeaponMesh->GetAnimInstance()->Montage_SetEndDelegate(EndDelegate);

		if (OwningCharacter) OwningCharacter->JustFired(WeaponType);
	}

	if(!bInfiniteMagAmmo) CurrentAmmoInMag--;
}

EGunStateEnum::Type ABaseWeapon::GetCurrentState()
{
	return CurrentState;
}

void ABaseWeapon::OnSwitched()
{
	UAnimInstance * AnimInstance = WeaponMesh->GetAnimInstance();

	if (!AnimInstance) return;

	if (AnimInstance->Montage_IsPlaying(FireMontage))
	{
		AnimInstance->Montage_Stop(0.0f, FireMontage);
	}
	else if (AnimInstance->Montage_IsPlaying(ReloadMontage))
	{
		AnimInstance->Montage_Stop(0.0f, ReloadMontage);
	}
}

bool ABaseWeapon::GetTraceStart(FVector& StartLoc, FRotator& StartRot)
{
	APawn* Player = Cast<APawn>(GetOwner());
	if (!Player) return false ;

	Player->GetController()->GetPlayerViewPoint(StartLoc, StartRot);
	return true;
}

void ABaseWeapon::EndAnimation(UAnimMontage* Montage, bool bInterrupted)
{
	if (CurrentState == EGunStateEnum::Type::GSE_Reloading && !bInterrupted)
	{	
		if (bInfiniteInventoryAmmo)
		{
			CurrentAmmoInMag = MaxAmmoInMag;
		}
		else
		{
			int32 OldCurrent = CurrentAmmoInMag;
			CurrentAmmoInMag = FMath::Min(CurrentAmmoInInventory + CurrentAmmoInMag, MaxAmmoInMag);
			CurrentAmmoInInventory -= FMath::Max(CurrentAmmoInMag - OldCurrent, 0);
		}

	}

	CurrentState = EGunStateEnum::Type::GSE_Idle;

	if (bWantsToFire && bAutomatic) Fire();
}

void ABaseWeapon::DealDamageTo(AActor* Target)
{
	if (CanDamage(Target))
	{
		Target->TakeDamage(DamagePerShot, FDamageEvent(), nullptr,nullptr);
	}
	//else we hit a wall or something
	else
	{

	}
}

void ABaseWeapon::SpawnEmitterAtLocation(const FHitResult& Hit)
{
	if (HitParticles && Hit.bBlockingHit)
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), HitParticles, Hit.Location, Hit.Normal.Rotation());
}

void ABaseWeapon::GiveAmmo(int32 Ammo)
{
	CurrentAmmoInInventory = FMath::Clamp(CurrentAmmoInInventory + Ammo, 0, MaxAmmoInInventory);
}

void ABaseWeapon::PlaySound(USoundCue* Play)
{
	if (Play) UGameplayStatics::PlaySoundAttached(Play, Root);
}

FString ABaseWeapon::EGunEnumToString(EGunEnum::Type EnumValue)
{
	const UEnum* EnumPtr = FindObject<UEnum>(ANY_PACKAGE, TEXT("EGunEnum::Type"), true);
	if (!EnumPtr) return FString("Invalid");

	return EnumPtr->GetEnumName(EnumValue); // for EnumValue == VE_Dance returns "VE_Dance"
}

float ABaseWeapon::GetReloadRate(float ReloadAnimTotal)
{
	if (!bCustomReloadTime) return 1.0f;
	return (ReloadAnimTotal / ReloadTime);
}
float ABaseWeapon::GetFireRate(float FireAnimTotal)
{
	if (!bCustomFireRate) return 1.0f;
	return (FireAnimTotal / FireRate);
}