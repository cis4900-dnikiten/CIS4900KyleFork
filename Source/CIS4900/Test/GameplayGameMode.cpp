// Fill out your copyright notice in the Description page of Project Settings.

#include "CIS4900.h"
#include "GameplayGameMode.h"

//default parts
#include "GameplayGameState.h"
#include "GameplayCharacter.h"
#include "GameplayPlayerController.h"
#include "GameplayHUD.h"

AGameplayGameMode::AGameplayGameMode()
{
	GameStateClass = AGameplayGameState::StaticClass();
	PlayerControllerClass = AGameplayPlayerController::StaticClass();
	DefaultPawnClass = AGameplayCharacter::StaticClass();
	HUDClass = AGameplayHUD::StaticClass();
}

