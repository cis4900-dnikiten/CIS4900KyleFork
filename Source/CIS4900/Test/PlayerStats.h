// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "CurrentMaxTuple.h"
#include "BaseWeapon.h"
#include "PlayerStats.generated.h"

#define PS_CURRENT_HEALTH_START 300
#define PS_CURRENT_ARMOR_START 200
#define PS_MAX_HEALTH_START 300
#define PS_MAX_ARMOR_START 200
#define PS_MAX_BATTERY_POWER 100.0f

class ABaseWeapon;
/**
 * 
 */
UCLASS()
class CIS4900_API UPlayerStats : public UObject
{
	GENERATED_BODY()

public:
	UPlayerStats();

	//Getters
	UFUNCTION(BlueprintCallable, Category = Getter)
		FCurrentMaxStruct GetGunMagAmmoByEnum(EGunEnum::Type WhatGun);
	UFUNCTION(BlueprintCallable, Category = Getter)
		FCurrentMaxStruct GetGunMagAmmoBySlot(uint8 WhatGun);
	UFUNCTION(BlueprintCallable, Category = Getter)
		FCurrentMaxStruct GetCurrentGunMagAmmo();
	UFUNCTION(BlueprintCallable, Category = Getter)
		int32 GetCurrentGunPickupAmmo();

	UFUNCTION(BlueprintCallable, Category = Getter)
		FCurrentMaxStruct GetGunInventoryAmmoByEnum(EGunEnum::Type WhatGun);
	UFUNCTION(BlueprintCallable, Category = Getter)
		FCurrentMaxStruct GetGunInventoryAmmoBySlot(uint8 WhatGun);
	UFUNCTION(BlueprintCallable, Category = Getter)
		FCurrentMaxStruct GetCurrentGunInventoryAmmo();
	UFUNCTION(BlueprintCallable, Category = Getter)
		EGunEnum::Type GetCurrentGunEnum();
	UFUNCTION(BlueprintCallable, Category = Getter)
		uint8 GetSlot(EGunEnum::Type T);

	UFUNCTION(BlueprintCallable, Category = Getter)
		FCurrentMaxStruct GetHealth();
	UFUNCTION(BlueprintCallable, Category = Getter)
		FCurrentMaxStruct GetArmor();

	UFUNCTION(BlueprintCallable, Category = Getter)
		FCurrentMaxStruct GetBatteryPower();

	UFUNCTION(BlueprintCallable, Category = Getter)
		bool HasWeaponBySlot(uint8 Slot);
	UFUNCTION(BlueprintCallable, Category = Getter)
		bool HasWeaponByEnum(EGunEnum::Type Gun);

	UFUNCTION(BlueprintCallable, Category = Getter)
		bool HasKey(EKeysEnum::Type Key);

	UFUNCTION(BlueprintCallable, Category = Getter)
		bool IsLowHealth();
	//

	//setters
	UFUNCTION(BlueprintCallable, Category = Setter)
		void SetHealthCurrentMax(FCurrentMaxStruct NewHealth);
	UFUNCTION(BlueprintCallable, Exec, Category = Setter)
		void SetHealth(int32 NewCurrentHealth);
	UFUNCTION(BlueprintCallable, Category = Setter)
		void AddHealth(int32 ToAdd);

	UFUNCTION(BlueprintCallable, Category = Setter)
		void SetArmorCurrentMax(FCurrentMaxStruct NewArmor);
	UFUNCTION(BlueprintCallable, Category = Setter)
		void SetArmor(int32 NewCurrentArmor);
	UFUNCTION(BlueprintCallable, Category = Setter)
		void AddArmor(int32 ToAdd);

	UFUNCTION(BlueprintCallable, Category = Setter)
		void AddAmmoCurrent(int32 Amount);
	UFUNCTION(BlueprintCallable, Category = Setter)
		void AddAmmoBySlot(int32 Amount, uint8 Slot);
	UFUNCTION(BlueprintCallable, Category = Setter)
		void AddAmmoByEnum(int32 Amount, EGunEnum::Type Gun);
	UFUNCTION(BlueprintCallable, Category = Setter)
		void GiveWeaponBySlot(uint8 Slot);
	UFUNCTION(BlueprintCallable, Category = Setter)
		void GiveWeaponByEnum(EGunEnum::Type Gun);

	UFUNCTION(BlueprintCallable, Category = Setter)
		void GiveKey(EKeysEnum::Type Key);

	UFUNCTION(BlueprintCallable, Category = Setter)
		void AddBatteryPower(int32 AddAmount);
	UFUNCTION(BlueprintCallable, Category = Setter)
		bool SubBatteryPower(int32 AddAmount);
	//

	void Load(class UGameplaySaveGame* Save);

	uint8 GetNextSlot(uint8 CurrentSlot,uint8 Incrementer = -1);

	void InitGunArray(UWorld* World, AActor* Owner, USceneComponent* FPMeshToAttachTo, USceneComponent* TPMeshToAttachTo);
	//Will make the weapon at the specified slot current and return it if the player has access to it
	//else it will return nullptr
	ABaseWeapon* MakeSlotCurrent(uint8 Slot);

	inline ABaseWeapon* GetCurrentWeapon() { return CurrentWeapon; }
	
protected:

	int32 HPCurrent;
	int32 HPMax;
	int32 ArmorCurrent;
	int32 ArmorMax;

	float LowHPThreshold;

	bool bHasKey1;
	bool bHasKey2;
	bool bHasKey3;

	float BatteryPowerCurrent;
	float BatteryPowerMax;

	TArray<ABaseWeapon*> WeaponInventory;
	//maps slot# to if player has the gun
	TMap<uint8,bool> HasWeapon;

	ABaseWeapon* CurrentWeapon;

private:
	bool bWasInitialized;

	ABaseWeapon* FindWeaponWithEnum(EGunEnum::Type Enum);
	ABaseWeapon* FindWeaponWithSlot(uint8 Slot);
};
