// Fill out your copyright notice in the Description page of Project Settings.

#include "CIS4900.h"
#include "Shotgun.h"

AShotgun::AShotgun() : PelletCount(20)
{
	WeaponType = EGunEnum::Type::GE_Shotgun;
	AttachSocketName_FPP = FName("S_Shotgun");
	AttachSocketName_TPP = FName("Shotgun");
	SlotNumber = 3;

	InaccuracyAngle = 15;
	DamagePerShot = 5.0f;

	bAutomatic = false;

	bCustomFireRate = true;
	FireRate = 0.7f;
}

void AShotgun::SuccesfulFire()
{
	Super::SuccesfulFire();

	FVector StartLoc;
	FRotator StartRot;

	if (!GetTraceStart(StartLoc, StartRot)) return;
	
	ECollisionChannel TraceChannel = GetWeaponTraceChannel();

	TArray<FHitResult> Hits = UCIS4900Statics::ConeTraceByChannel(GetWorld(), StartLoc, StartRot, LineTraceDistance ,PelletCount, InaccuracyAngle, TraceChannel, TraceParams);

	for (auto i : Hits)
	{
		if (i.bBlockingHit)
		{
			DealDamageTo(i.GetActor());
		}

		SpawnEmitterAtLocation(i);
	}
}

