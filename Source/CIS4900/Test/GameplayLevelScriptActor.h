// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/LevelScriptActor.h"
#include "GameplayLevelScriptActor.generated.h"

/**
 * 
 */
UCLASS()
class CIS4900_API AGameplayLevelScriptActor : public ALevelScriptActor
{
	GENERATED_BODY()

	void BeginPlay() override;
	
protected:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Functionality)
		bool bShouldLoadOnBegin;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Functionality)
		FName NextLevel;
};
