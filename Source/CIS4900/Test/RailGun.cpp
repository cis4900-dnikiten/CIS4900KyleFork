// Fill out your copyright notice in the Description page of Project Settings.

#include "CIS4900.h"
#include "GameplayCharacter.h"
#include "RailGun.h"

FRailGunShotParams::FRailGunShotParams(float PercentCharged, int32 BulletDamage, int32 MaxBounces, int32 MaxPierce, 
	UParticleSystem* L1, UParticleSystem*L2, UParticleSystem* L3, UParticleSystem* L4) : bBounce(false), TimesToBounce(5), bPierce(false)
{
	Damage = BulletDamage;
	TimesToBounce = MaxBounces;
	TimesToPierce = MaxPierce;

	CurrentSystem = L1;

	if (PercentCharged * 100 >= ARailGun::Threshold1)
	{
		/*This threshold will allow the bullet to hit multiple enemies*/
		bPierce = true;
		CurrentSystem = L2;
	}

	if (PercentCharged * 100 >= ARailGun::Threshold2)
	{
		/*This threshold will double the damage*/
		Damage *= 2;
		CurrentSystem = L3;
	}

	if (PercentCharged * 100 >= ARailGun::Threshold3)
	{
		/*This threshold will allow the bullet to bounce at certain angles*/
		bBounce = true;
		CurrentSystem = L4;
	}
}

ARailGun::ARailGun() : ChargeDelay(0.1f), ChargeAmount(1), MaxBounces(5), MaxPierce(5)
{
	SlotNumber = 4;
	WeaponType = EGunEnum::Type::GE_RailGun;
	AttachSocketName_FPP = FName("S_RailGun");
	AttachSocketName_TPP = FName("RailGun");

	MaxAmmoInMag = 100;
	CurrentAmmoInMag = 0;
	MaxAmmoInInventory = 10;
	bInfiniteInventoryAmmo = false;
	bAutomatic = false;
}

uint8 ARailGun::GetThreshold1()
{
	return ARailGun::Threshold1;
}
uint8 ARailGun::GetThreshold2()
{
	return ARailGun::Threshold2;
}
uint8 ARailGun::GetThreshold3()
{
	return ARailGun::Threshold3;
}

void ARailGun::BeginPlay()
{
	Super::BeginPlay();

	CurrentAmmoInMag = 0;

	AnimInstance = WeaponMesh->GetAnimInstance();
}

void ARailGun::MakeBeam(FVector SourceVector, FVector TargetVector, UParticleSystem* BeamSystem)
{
	UParticleSystemComponent* BeamEmitter = UGameplayStatics::SpawnEmitterAttached(BeamSystem, WeaponMesh, AttachSocketName_Muzzle);
	if (BeamEmitter)
	{
		FName SourceName("BeamSource");
		FName TargetName("BeamTarget");

		FParticleSysParam Source, Target;
		Source.Name = SourceName;
		Source.ParamType = EParticleSysParamType::PSPT_Vector;
		Source.Vector = SourceVector;
		BeamEmitter->InstanceParameters.Add(Source);

		Target.Name = TargetName;
		Target.ParamType = EParticleSysParamType::PSPT_Vector;
		Target.Vector = TargetVector;
		BeamEmitter->InstanceParameters.Add(Target);

		BeamEmitter->ActivateSystem(true);
	}
}

void ARailGun::StartFire()
{
	if (FireMontage && AnimInstance && AnimInstance->Montage_IsPlaying(FireMontage)) return;

	if (HaveAmmo())
	{
		GetWorldTimerManager().SetTimer(ChargeHandle, this, &ARailGun::Charge, ChargeDelay, true);
		StartChargeAnimation();
	}
	else
	{
		PlaySound(EmptySound);
	}
}
void ARailGun::StopFire()
{
	if (GetWorldTimerManager().IsTimerActive(ChargeHandle))
	{
		EndChargeAnimation();
		ReleaseCharge();
		GetWorldTimerManager().ClearTimer(ChargeHandle);
		if (OwningCharacter) OwningCharacter->JustFired(WeaponType);
	}
}
void ARailGun::OnSwitched()
{
	Super::OnSwitched();
	StopFire();
}


void ARailGun::ReleaseCharge()
{


	FVector StartLoc;
	FRotator StartRot;
	if (!GetTraceStart(StartLoc, StartRot)) return;

	FVector BeamStartLoc = WeaponMesh->GetSocketLocation(AttachSocketName_Muzzle);
	FRailGunShotParams ShotParams((float)CurrentAmmoInMag / (float)MaxAmmoInMag, DamagePerShot, MaxBounces, MaxPierce, Level1BeamSystem, Level2BeamSystem, Level3BeamSystem, Level4BeamSystem);
	TArray<FHitResult> Hits;
	
	int32 TimesBounced = -1;
	
	bool bHitBouncingSurface;

	do
	{
		//was the last hit something we can bounce off of
		bHitBouncingSurface = false;

		TimesBounced++;

		Hits = PierceTrace(StartLoc, StartRot, ShotParams.bPierce);
		MakeBeam(BeamStartLoc, Hits.Last().Location,ShotParams.CurrentSystem);

		for (auto i : Hits)
		{
			//If any of the hits can be damaged
			if (i.bBlockingHit && CanDamage(i.GetActor()))
			{
				i.GetActor()->TakeDamage(ShotParams.Damage, FDamageEvent(), nullptr, nullptr);
			}
			//If we are at the last hit and it could not be damaged
			else if (i.bBlockingHit && i.GetActor() == Hits.Last().GetActor())
			{
				bHitBouncingSurface = true;

				CalculateBounce(StartRot, StartRot.Vector(), i);
				StartLoc = i.Location;
				BeamStartLoc = StartLoc;
			}

			SpawnEmitterAtLocation(i);
		}
		//If we can bounce and we havent bounced max times
	} while (bHitBouncingSurface && ShotParams.bBounce && ShotParams.TimesToBounce > TimesBounced);

	if (!bInfiniteInventoryAmmo) CurrentAmmoInInventory = FMath::Clamp(CurrentAmmoInInventory - 1, 0, MaxAmmoInInventory);
	CurrentAmmoInMag = 0;
}

TArray<FHitResult> ARailGun::PierceTrace(const FVector& StartLoc, const FRotator& StartRot, bool Pierce)
{
	TArray<FHitResult> Ret;
	
	FVector Location = StartLoc;
	FRotator Rotation = StartRot;

	FCollisionQueryParams TraceParamsSave = TraceParams;

	do
	{
	
		FHitResult Hit(ForceInit);
		Trace(Location, Rotation, Hit);

		Ret.Add(Hit);

		if (Hit.bBlockingHit && CanPierce(Hit.GetActor()))
		{
			Location = Hit.Location;
			TraceParams.AddIgnoredActor(Hit.GetActor());
		}
		else
		{
			break;
		}

	} while (Pierce && MaxPierce > Ret.Num()-1);

	TraceParams = TraceParamsSave;

	return Ret;
}

bool ARailGun::CanPierce(AActor* Other)
{
	//Here incase we wantto add a reflective shield enemy or something
	return CanDamage(Other);
}

void ARailGun::CalculateBounce(FRotator& NewDirection, const FVector& VectorToBounce, const FHitResult& Hit)
{
	FVector NewVector = -2.0f * FVector::DotProduct(VectorToBounce, Hit.Normal) * Hit.Normal + VectorToBounce;
	NewDirection = NewVector.Rotation();
}

bool ARailGun::Reload()
{
	return true;
}

void ARailGun::Charge()
{

	CurrentAmmoInMag = FMath::Clamp<int32>(CurrentAmmoInMag + ChargeAmount, 0, MaxAmmoInMag);
}

//PRIVATE
void ARailGun::StartChargeAnimation()
{
	if (ChargeMontage && AnimInstance)
	{
		AnimInstance->Montage_Play(ChargeMontage);
	}
}
void ARailGun::EndChargeAnimation()
{
	if (!AnimInstance) return;

	if (ChargeMontage && AnimInstance->Montage_IsPlaying(ChargeMontage))
	{
		AnimInstance->Montage_Stop(0.0f, ChargeMontage);
	}

	if (FireMontage)
	{
		float Time = AnimInstance->Montage_Play(FireMontage);
		if(bCustomFireRate) AnimInstance->Montage_SetPlayRate(FireMontage, GetFireRate(Time));
	}
}