// Fill out your copyright notice in the Description page of Project Settings.

#include "CIS4900.h"
#include "GridTile.h"


// Sets default values
UGridTile::UGridTile()
{
	TileSize = 400.0f;
}

float UGridTile::GetTileSize()
{
	return TileSize;
}

