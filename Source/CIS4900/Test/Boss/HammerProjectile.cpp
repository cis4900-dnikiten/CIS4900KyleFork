// Fill out your copyright notice in the Description page of Project Settings.

#include "CIS4900.h"
#include "GridTile.h"
#include "../GameplayCharacter.h"
#include "HammerProjectile.h"


// Sets default values
AHammerProjectile::AHammerProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(FName("Root"));
	SetRootComponent(Root);

	Mesh = CreateDefaultSubobject<USkeletalMeshComponent>(FName("Mesh"));
	Mesh->AttachTo(GetRootComponent());

	ProjComp = CreateDefaultSubobject<UProjectileMovementComponent>(FName("ProjectileMovement"));

	ProjComp->bIsHomingProjectile = true;
	ProjComp->HomingAccelerationMagnitude = 300.0f;
	ProjComp->MaxSpeed = 3000.0f;
	ProjComp->InitialSpeed = 3000.0f;

	OnActorBeginOverlap.AddDynamic(this, &AHammerProjectile::OnOverlap);
}

// Called when the game starts or when spawned
void AHammerProjectile::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AHammerProjectile::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void AHammerProjectile::FlyTo(USceneComponent* Target)
{
	ProjComp->HomingTargetComponent = Target;
}

void AHammerProjectile::OnOverlap(AActor* Other)
{
	if (Other == GetOwner()) return;

	AGameplayCharacter * Player = Cast<AGameplayCharacter>(Other);

	if (Player)
	{
		if (Player->bCanBeDamaged)
		{
			Player->TakeDamage(Damage, FDamageEvent(), nullptr, this);
		}
		
		FVector Direction = (Player->GetActorLocation() - GetActorLocation());
		Direction = FVector(Direction.X, Direction.Y, 0.0f);

		FRotator Rot = FRotationMatrix::MakeFromX(Direction).Rotator();
		Rot = FRotator(FMath::Clamp(LaunchAngle, 0.0f, 90.0f), Rot.Yaw, Rot.Roll);
		FVector LaunchDir = Rot.Vector();
		LaunchDir.Normalize();
		Player->LaunchCharacter(LaunchDir*LaunchForce, true, true);
	}

	Destroy();
}
