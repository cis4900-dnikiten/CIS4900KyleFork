// Fill out your copyright notice in the Description page of Project Settings.

#include "CIS4900.h"
#include "../GameplayPlayerController.h"
#include "../GameplayHUD.h"
#include "GridTile.h"
#include "Grid.h"


// Sets default values
AGrid::AGrid()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	GRoot = CreateDefaultSubobject<USceneComponent>(FName("Root"));
	SetRootComponent(GRoot);

	OldN = N = 0;

	//BuildGrid();
}

void AGrid::OnConstruction(const FTransform & Trans)
{
	Super::OnConstruction(Trans);


	if (OldN != N)
	{
		bBuilt = false;
		OldN = N;
	}

	BuildGrid();
}


void AGrid::BuildGrid()
{
	if (bBuilt)
	{
		return;
	}

	auto Attached = GetComponentsByClass(TileToSpawn);

	if (Attached.Num() != 0)
	{ 
		for (auto i : Attached)
		{
			i->DestroyComponent();
		}
	}

	for (int32 i = 0; i < N; i++)
	{
		for (int32 j = 0; j < N; j++)
		{
			UGridTile* NewTile = NewObject<UGridTile>(this, *TileToSpawn);

			if (NewTile)
			{
				NewTile->RegisterComponent();

				StartX = -(NewTile->GetTileSize()*N/2);
				StartY = -(NewTile->GetTileSize()*N/2);

				FVector SpawnLoc = FVector(StartX + i*NewTile->GetTileSize(), StartY + j*NewTile->GetTileSize(), 0);

				NewTile->AttachTo(GetRootComponent());
				NewTile->SetRelativeLocation(SpawnLoc);
			}
		}

	}

	bBuilt = true;
}

// Called when the game starts or when spawned
void AGrid::BeginPlay()
{
	Super::BeginPlay();
	BuildGrid();
}

// Called every frame
void AGrid::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void AGrid::ShakePlayerHUD()
{
	AGameplayHUD* PHUD = GetPlayerHUD();

	if (PHUD)
	{
		PHUD->Tremor();
	}
}

UGridTile* AGrid::GetClosestTileTo_Implementation(const FVector & P)
{
	FHitResult Hit;
	
	FVector Point = P;
	Point.X = FMath::Clamp(Point.X, StartX, -1 * StartX);
	Point.Y = FMath::Clamp(Point.Y, StartY, -1 * StartY);

	GetWorld()->LineTraceSingleByChannel(Hit, Point + FVector(0.0f, 0.0f, 100.0f), Point + FVector(0.0f, 0.0f, -100.0f), ECC_Visibility);

	UE_LOG(LogTemp, Warning, TEXT("%s"), *Hit.GetActor()->GetName());

	return Cast<UGridTile>(Hit.GetComponent());
}

AGameplayHUD* AGrid::GetPlayerHUD_Implementation()
{
	AGameplayPlayerController* PController = Cast<AGameplayPlayerController>(GetWorld()->GetFirstPlayerController());
	
	AGameplayHUD * Ret = nullptr;
	
	if (PController)
	{
		Ret = Cast<AGameplayHUD>(PController->GetHUD());
	}
	return Ret;
}
