// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "../CurrentMaxTuple.h"
#include "Lavalord.generated.h"



UCLASS()
class CIS4900_API ALavalord : public ACharacter
{
	GENERATED_BODY()

public:
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		UBoxComponent* DamageTrigger;
		
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		USkeletalMeshComponent* Hammer;

public:
	// Sets default values for this character's properties
	ALavalord();

	/*A reference to the player*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Functionality)
		class AGameplayCharacter * Enemy;

	/*If the combat animation logic should be used*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Functionality)
		bool bInCombat;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser) override;


	UFUNCTION(BlueprintCallable, Category = Functionality)
		void StartCharge();

	UFUNCTION(BlueprintCallable, Category = Functionality)
		void EndCharge();

	UFUNCTION(BlueprintCallable, Category = Functionality)
		void ThrowHammer();

	UFUNCTION(BlueprintCallable, Category = Functionality)
		void PickupHammer();

	UFUNCTION(BlueprintCallable, Category = Functionality)
		void WeakAttack();
	/*Asks the grid for the closest tile to the player, and returns the center point of that tile*/
	UFUNCTION(BlueprintCallable, Category = Functionality)
		FVector GetHammerThrowPoint();

	UFUNCTION(BlueprintCallable, Category = Animations)
		FCurrentMaxStruct GetHealth();

	/*Animation blueprint callbacks, to know when the animation is over*/
	UFUNCTION(BlueprintCallable, Category = Animations)
		void EndWeakAttackCallback();
	UFUNCTION(BlueprintCallable, Category = Animations)
		void WeakAttackApexCallback();
	UFUNCTION(BlueprintCallable, Category = Animations)
		void WeakAttackStopDamageCallback();

	UFUNCTION(BlueprintCallable, Category = Animations)
		void ThrowHammerApexCallback();
	UFUNCTION(BlueprintCallable, Category = Animations)
		void ThrowHammerEndCallback();

	UFUNCTION(BlueprintCallable, Category = Animations)
		void EndPickupCallback();

	UFUNCTION(BlueprintCallable, Category = Animations)
		void EndChargeAttackCallback();

	UFUNCTION(BlueprintCallable, Category = Functionality)
		ELavalordState::Type GetCurrentState();

	UPROPERTY(BlueprintReadOnly, Category = Functionality)
		bool bWeakAttack;
	UPROPERTY(BlueprintReadOnly, Category = Functionality)
		bool bChargeAttack;
	UPROPERTY(BlueprintReadOnly, Category = Functionality)
		bool bThrowingHammer;

protected:

	TEnumAsByte<ELavalordState::Type> CurrentState;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Functionality)
		FName WeaponSocketName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Functionality)
		float WeakAttackDamage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Functionality)
		float PlayerLaunchForce;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Functionality)
		float PlayerLaunchAngle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Functionality)
		float WalkingSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Functionality)
		float ChargingSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Functionality)
		TSubclassOf<class AHammerProjectile> HammerToSpawn;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Functionality)
		float MaxHealth;

	UFUNCTION(BlueprintNativeEvent, Category = Functionality)
		void Die();

	UFUNCTION(BlueprintCallable, Category = Functionality)
		void SetState(ELavalordState::Type New);

	bool bDidDamageThisSwing;
	
	float CurrentHealth;
	bool bDamageable;

	/*Do damage and apply a force to the player if he is in the trigger*/
	void TriggerImpulse();

private:
	class AHammerProjectile* SpawnedHammer;
};
