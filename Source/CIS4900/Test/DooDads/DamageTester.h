// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Pawn.h"
#include "DamageTester.generated.h"

UCLASS()
class CIS4900_API ADamageTester : public APawn
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, Category = Mesh)
	USceneComponent* Root;

	UPROPERTY(VisibleAnywhere, Category = Mesh)
	UStaticMeshComponent* VisibleMesh;

	UPROPERTY(VisibleAnywhere, Category = Mesh)
	UTextRenderComponent* HealthDisplayText;

	UPROPERTY(VisibleAnywhere, Category = Mesh)
	UTextRenderComponent* DamageDisplayText;

public:
	// Sets default values for this pawn's properties
	ADamageTester();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//called to take damage
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser) override;

protected:
	float Health;
};
