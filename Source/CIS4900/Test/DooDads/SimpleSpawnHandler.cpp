// Fill out your copyright notice in the Description page of Project Settings.

#include "CIS4900.h"
#include "SimpleSpawnHandler.h"

ASimpleSpawnHandler::ASimpleSpawnHandler()
{
	ActorsToSpawn = 1;
	ActorsSpawned = 0;
}

bool ASimpleSpawnHandler::HasMoreToSpawn_Implementation()
{
	return (ActorsToSpawn > ActorsSpawned);
}

AActor* ASimpleSpawnHandler::NextActor_Implementation()
{
	ActorsSpawned++;
	auto ToSpawn = GetRandomActor();

	FActorSpawnParameters SpawnParams;
	SpawnParams.bNoCollisionFail = true;

	return (ToSpawn) ? GetWorld()->SpawnActor<AActor>(*ToSpawn,SpawnParams) : nullptr;
}

void ASimpleSpawnHandler::StopSpawningProcess_Implementation()
{
	ActorsSpawned = 0;
}

TSubclassOf<AActor> ASimpleSpawnHandler::GetRandomActor()
{
	int32 Index = FMath::RandRange(0, ToSpawn.Num() - 1);
	
	return Index > -1 && Index < ToSpawn.Num() ? ToSpawn[Index] : nullptr;
}