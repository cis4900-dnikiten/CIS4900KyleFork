// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/TriggerBox.h"
#include "SpawnHandler.h"
#include "SpawningTrigger.generated.h"

UENUM(BlueprintType, Category = Enum)
namespace EBurstEnum
{
	enum Type
	{
		/*Will start at the top of the list and go down, cyclical*/
		BE_Linear,
		/*Will randomly get elements from the list*/
		BE_Random
	};
}

/**
 * 
 */
UCLASS()
class CIS4900_API ASpawningTrigger : public ATriggerBox
{
	GENERATED_BODY()
	
	
public:
	ASpawningTrigger();

	/*Function to call to initiate the spawning process, call it with false to stop it*/
	UFUNCTION(BlueprintCallable, Category = Functionality)
		void Trigger(bool bStart = true);
	
	/*True if currently processing bursts, false otherwise*/
	UFUNCTION(BlueprintCallable, Category = Functionality)
		bool IsActive();

	virtual void BeginPlay() override;

	virtual void Tick(float Delta) override;

protected:
	/*The list of SpawnHandlers to use during bursts*/
	UPROPERTY(EditAnywhere, Category = Functionality)
		TArray<TSubclassOf<ASpawnHandler>> BurstList;

	UPROPERTY(EditAnywhere, Category = Functionality)
		TEnumAsByte<EBurstEnum::Type> BurstMethod;

	/*The amount of spawners to use from BurstList per burst*/
	UPROPERTY(EditAnywhere, Category = Functionality, meta = (ClampMin = "1", ClampMax = "10"))
		uint8 HandlersPerBurst;

	/*Amount of bursts to do, 0 is infinite*/
	UPROPERTY(EditAnywhere, Category = Functionality)
		uint8 Bursts;

	/*The amount of time inbetween bursts*/
	UPROPERTY(EditAnywhere, Category = Functionality)
		float TimeBetweenBursts;

	/*If the spawner should wait for TimeBetweenBursts time for the first burst*/
	UPROPERTY(EditAnywhere, Category = Functionality)
		bool bWaitFirstBurst;

	/*If the spawner should trigger itself on BeginPlay*/
	UPROPERTY(EditAnywhere, Category = Functionality)
		bool bStartActivated;

	/*If the spawner should only ever be triggered once*/
	UPROPERTY(EditAnywhere, Category = Functionality)
		bool bOnlyTriggerOnce;

	FTimerHandle BurstHandle;

	void StartProcess();
	void StopProcess();

	UFUNCTION()
		void Burst();
	
	UPROPERTY(EditAnywhere, Category = Functionality)
	TArray<ASpawnHandler*> InstancedHandlers;

private:
	uint8 CurrentBurst;
	uint8 CurrentHandlerIndex;
	uint8 HandlersHandled;

	bool bHasBeenTriggered;

	bool ShouldBurst();
	bool ContinueBurst();

	ASpawnHandler* GetNextSpawnHandler();
	void HandleSpawnHandler(ASpawnHandler* ToHandle);

	FVector GetSpawnLocation();
};
