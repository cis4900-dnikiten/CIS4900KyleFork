// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/SceneComponent.h"
#include "SpawnHandler.generated.h"


UCLASS()
class CIS4900_API ASpawnHandler : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	ASpawnHandler();
	// Called when the game starts
	virtual void BeginPlay() override;

	//TODO: figure out how to make an iterator, so this can be used with a for each loop

	/*If this spawner wants to spawn more actors*/
	UFUNCTION(BlueprintNativeEvent, Category = Functionality)
	bool HasMoreToSpawn();
	virtual bool HasMoreToSpawn_Implementation();

	/*This will spawn the actor and return a reference to it*/
	UFUNCTION(BlueprintNativeEvent, Category = Functionality)
	AActor* NextActor();
	virtual AActor* NextActor_Implementation();

	/*This will be called when the spawning process wants to be started, state should be cleared here*/
	UFUNCTION(BlueprintNativeEvent, Category = Functionality)
	void StartSpawningProcess();
	virtual void StartSpawningProcess_Implementation();

	/*Called if the spawning process needs to be prematurely stopped before all desired actors have spawned*/
	UFUNCTION(BlueprintNativeEvent, Category = Functionality)
	void StopSpawningProcess();
	virtual void StopSpawningProcess_Implementation();
};
