// Fill out your copyright notice in the Description page of Project Settings.

#include "CIS4900.h"
#include "DamageTester.h"

#define LOCTEXT_NAMESPACE "DamageTesterSpace" 

// Sets default values
ADamageTester::ADamageTester() : Health(1000)
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	SetRootComponent(Root);

	VisibleMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisibleMesh"));
	VisibleMesh->AttachTo(GetRootComponent());
	
	DamageDisplayText = CreateDefaultSubobject<UTextRenderComponent>(TEXT("DamageTextRender"));
	DamageDisplayText->SetText(LOCTEXT("DamageDisplayStr", "Damage me"));
	DamageDisplayText->AttachTo(GetRootComponent());
	
	HealthDisplayText = CreateDefaultSubobject<UTextRenderComponent>(TEXT("HealthTextRender"));
	HealthDisplayText->SetText(FText::Format(LOCTEXT("HealthDisplayStr", "I have {0} Health"), FText::AsNumber(Health)));
	HealthDisplayText->AttachTo(GetRootComponent());
}

// Called when the game starts or when spawned
void ADamageTester::BeginPlay()
{
	Super::BeginPlay();
}

float ADamageTester::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser)
{
	if (Health == 0)
	{
		Health = 1000;
	}

	Health = FMath::Clamp<float>(Health - DamageAmount, 0.0f, 1000.0f);

	HealthDisplayText->SetText(FText::Format(LOCTEXT("HealthDisplayStr","I have {0} Health"), FText::AsNumber(Health)) );
	DamageDisplayText->SetText(FText::Format(LOCTEXT("DamageDisplayStr", "I took {0} damage"), FText::AsNumber(DamageAmount)));

	return DamageAmount;
}

#undef LOCTEXT_NAMESPACE