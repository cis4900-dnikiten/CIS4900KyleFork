// Fill out your copyright notice in the Description page of Project Settings.

#include "CIS4900.h"
#include "DamageTrigger.h"


// Sets default values
ADamageTrigger::ADamageTrigger() : DamagePerTick(0.0f), bTick(true), TickTime(1.0f)
{
	//Root = CreateDefaultSubobject<USceneComponent>(TEXT("DTRoot"));
	//SetRootComponent(Root);

	CollisionMesh = CreateDefaultSubobject <UBoxComponent>(TEXT("CollisionMesh"));
	CollisionMesh->InitBoxExtent(FVector(100.0f, 100.0f, 10.0f));
	CollisionMesh->SetCollisionResponseToChannel(WEAPON_TRACE_CHANNEL, ECR_Ignore);

	//CollisionMesh->AttachTo(GetRootComponent());

	SetActorEnableCollision(true);

	OnActorBeginOverlap.AddDynamic(this, &ADamageTrigger::OnActorStartOverlap);
	OnActorEndOverlap.AddDynamic(this, &ADamageTrigger::OnActorStopOverlap);

	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

void ADamageTrigger::DealDamage(AActor* Target)
{
	if (Target && IsValid(Target))
	{
		Target->TakeDamage(DamagePerTick, FPointDamageEvent(), nullptr, this);
	}
	else
	{
		GetWorldTimerManager().ClearTimer(TimerHandler[Target]);
		TimerHandler.Remove(Target);
	}
}

void ADamageTrigger::OnActorStartOverlap(AActor* Actor)
{
	FTimerHandle NewHandle;
	GetWorldTimerManager().SetTimer(NewHandle, FTimerDelegate::CreateUObject(this, &ADamageTrigger::DealDamage, Actor), TickTime, bTick, 0.0f );

	TimerHandler.Add(Actor, NewHandle);

}
void ADamageTrigger::OnActorStopOverlap(AActor* Actor)
{
	GetWorldTimerManager().ClearTimer(TimerHandler[Actor]);
	TimerHandler.Remove(Actor);
}
