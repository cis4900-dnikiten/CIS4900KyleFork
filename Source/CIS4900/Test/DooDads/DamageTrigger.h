// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "DamageTrigger.generated.h"

UCLASS()
class CIS4900_API ADamageTrigger : public AActor
{
	GENERATED_BODY()
	
	UPROPERTY(VisibleAnywhere, Category = Mesh)
	class UBoxComponent* CollisionMesh;

	//UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	//class USceneComponent * Root;

public:	
	// Sets default values for this actor's properties
	ADamageTrigger();

	UFUNCTION()
	virtual void DealDamage(class AActor* Target);
	UFUNCTION()
	void OnActorStartOverlap(class AActor* Actor);
	UFUNCTION()
	void OnActorStopOverlap(class AActor* Actor);
	
protected:
	UPROPERTY(EditAnywhere, Category = Setter)
	float DamagePerTick;
	UPROPERTY(EditAnywhere, Category = Setter)
	float TickTime;
	UPROPERTY(EditAnywhere, Category = Setter)
	bool bTick;

	TMap<class AActor*, FTimerHandle> TimerHandler;
};
