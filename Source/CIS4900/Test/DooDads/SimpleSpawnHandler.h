// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Test/DooDads/SpawnHandler.h"
#include "SimpleSpawnHandler.generated.h"

/**
 * 
 */
UCLASS()
class CIS4900_API ASimpleSpawnHandler : public ASpawnHandler
{
	GENERATED_BODY()

public:
	ASimpleSpawnHandler();

	/*If this spawner wants to spawn more actors*/
	virtual bool HasMoreToSpawn_Implementation() override;
	/*This will spawn the actor and return a reference to it*/
	virtual AActor* NextActor_Implementation() override;
	/*Called if the spawning process needs to be prematurely stopped before all desired actors have spawned*/
	virtual void StopSpawningProcess_Implementation() override;
	
protected:
	UPROPERTY(BlueprintReadWrite, Category = Functionality)
	int32 ActorsSpawned;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Functionality)
	TArray<TSubclassOf<AActor>> ToSpawn;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Functionality)
	int32 ActorsToSpawn;

	UFUNCTION(BlueprintCallable, Category = Functionality)
	TSubclassOf<AActor> GetRandomActor();



private:

};
