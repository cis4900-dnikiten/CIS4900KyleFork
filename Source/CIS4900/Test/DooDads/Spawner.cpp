// Fill out your copyright notice in the Description page of Project Settings.

#include "CIS4900.h"
#include "Spawner.h"


// Sets default values
ASpawner::ASpawner() : bWaitFirstTime(false), SpawnTime(1.0f)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	SetRootComponent(Root);

	CollisionMesh = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionMesh"));
	CollisionMesh->AttachTo(GetRootComponent());

	SpawnedActor = nullptr;
}

// Called when the game starts or when spawned
void ASpawner::BeginPlay()
{
	Super::BeginPlay();

	float FirstTimeWait = -1.0f;
	if (!bWaitFirstTime)
	{
		FirstTimeWait = 0.0f;
	}

	GetWorldTimerManager().SetTimer(Handle, this, &ASpawner::Spawn, SpawnTime, false, FirstTimeWait);
}

// Called every frame
void ASpawner::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	if (!IsValid(SpawnedActor) && !GetWorldTimerManager().IsTimerActive(Handle)) GetWorldTimerManager().SetTimer(Handle, this, &ASpawner::Spawn, SpawnTime, false);
}

void ASpawner::Spawn()
{
	
	SpawnedActor = GetWorld()->SpawnActor<AActor>(ToSpawn, GetTransform().GetLocation(), GetTransform().Rotator());
	
}
