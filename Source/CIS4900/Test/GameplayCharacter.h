// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "ConsoleManager.h"
#include "BaseWeapon.h"
#include "GameplayCharacter.generated.h"

//static TAutoConsoleVariable<float> CVarHealth(TEXT("Health"), PS_CURRENT_HEALTH_START, TEXT("Set player health"), ECVF_Cheat);
//static TAutoConsoleVariable<float> CVarArmor(TEXT("Armor"), PS_CURRENT_ARMOR_START, TEXT("Set player armor"), ECVF_Cheat);

#define ARMOR_PERCENT 0.9f

class UPlayerStats;

UCLASS()
class CIS4900_API AGameplayCharacter : public ACharacter
{
	GENERATED_BODY()

	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USkeletalMeshComponent* FPMesh;

	UPROPERTY(VisibleAnywhere, Category = Mesh)
	class USceneComponent* ShotStart;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent * FPCameraComponent;

	UPROPERTY(VisibleDefaultsOnly, Category= Mesh)
	class USpotLightComponent* FLight;

public:
	// Sets default values for this character's properties
	AGameplayCharacter();

	//called to take damage
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser) override;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	UFUNCTION(BlueprintCallable, Category = Getter)
	UPlayerStats * GetStats();

	UFUNCTION(BlueprintCallable, Category = Functionality)
		void GiveWeaponByEnum(EGunEnum::Type T);

	//Will give the specified gun, if player already has it, it will add ammo instead
	void GiveGun(EGunEnum::Type WhatGun);
	//will try to equip the gun that goes in slot Slot
	void EquipWeapon(uint8 Slot);

	//weapon calls this back so the character can play an animation
	void JustFired(EGunEnum::Type WhatGun);

	void Sprint();
	void StopSprinting();

	inline void Slot1(){ EquipWeapon(1); }
	inline void Slot2(){ EquipWeapon(2); }
	inline void Slot3(){ EquipWeapon(3); }
	inline void Slot4(){ EquipWeapon(4); }

	//A function for making getting the current weapon faster
	UFUNCTION(BlueprintCallable, Category = Getter)
	ABaseWeapon* GetCurrentWeapon();

	void GetShotStartPoint(FVector& Loc, FRotator& Rot);

	/*Time between charging/draining in seconds*/
	UPROPERTY(EditDefaultsOnly, Category = Functionality)
		float FlashlightTickTime;

	/*Amount to drain from the battery per tick if on*/
	UPROPERTY(EditDefaultsOnly, Category = Functionality)
		int32 BatteryDrainPerTick;

	/*Amount to add to the battery per tick if its not on*/
	UPROPERTY(EditDefaultsOnly, Category = Functionality)
		int32 BatteryChargePerTick;

	UPROPERTY(EditDefaultsOnly, Category = Functionality)
		int32 MaxWalkSpeed;
	UPROPERTY(EditDefaultsOnly, Category = Functionality)
		int32 MaxSprintSpeed;

	UPROPERTY(EditDefaultsOnly, Category = Functionality)
		int32 UseDistance;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Functionality)
		bool bInCinematic;

	static void SaveDefault(class UGameplaySaveGame* ToSaveTo);
	void Load(class UGameplaySaveGame* Save);
	void Save(class UGameplaySaveGame* Save);
protected:

	void ChangeWeaponSlot(float Incrementer);
	void ChangeWeaponSlotDown();
	void ChangeWeaponSlotUp();
	void Flashlight();
	void MoveForward(float Value);
	void MoveRight(float Value);
	void StartFire();
	void StopFire();
	void Reload();
	void Die();
	void Use();

	bool bNoDamage;

	UFUNCTION()
		void FlashlightTick();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Setter)
		UAnimMontage* FireAnimation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Setter)
		TArray<uint8> StartingWeapons;

	UPROPERTY(EditDefaultsOnly, Category = Functionality)
		USoundCue * FlashlightToggleSound;

	TEnumAsByte<EFlashlightEnum::Type> FlashlightState;
	FTimerHandle FlashlightHandle;

	class UObject* ToUse;

	//Console Stuff///////////////////////////////////////////////////////////
	UFUNCTION(Exec) void ConsoleAddHealth(const TArray<FString>& Params);
	UFUNCTION(Exec) void ConsoleAddArmor(const TArray<FString>& Params);
	UFUNCTION(Exec) void ConsoleAddAmmo(const TArray<FString>& Params);
	UFUNCTION(Exec) void ConsoleGiveGun(const TArray<FString>& Params);
	UFUNCTION(Exec) void ConsoleInfiniteAmmo(const TArray<FString>& Params);
	UFUNCTION(Exec) void ConsoleInfiniteHealth(const TArray<FString>& Params);

	FConsoleCommandWithArgsDelegate CFuncAddHealth;
	FConsoleCommandWithArgsDelegate CFuncAddArmor;
	FConsoleCommandWithArgsDelegate CFuncAddAmmo;
	FConsoleCommandWithArgsDelegate CFuncGiveGun;
	FConsoleCommandWithArgsDelegate CFuncInfiniteAmmoInventory;
	FConsoleCommandWithArgsDelegate CFuncInfiniteHealth;
	//END CONSOLE STUFF///////////////////////////////////////////////////////

private:

	void AddStartingWeapons();

	void CheckCVars();

	UPlayerStats * Stats;
	
	bool Usable(UObject * ToCheck);
};
