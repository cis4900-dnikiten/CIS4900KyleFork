// Fill out your copyright notice in the Description page of Project Settings.

#include "CIS4900.h"
#include "GameplayCharacter.h"
#include "BasePickup.h"


// Sets default values
ABasePickup::ABasePickup()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CollisionMesh = CreateDefaultSubobject<USphereComponent>(TEXT("Root"));
	CollisionMesh->InitSphereRadius(10.0f);
	CollisionMesh->SetCollisionResponseToChannel(WEAPON_TRACE_CHANNEL, ECR_Ignore);
	
	SetRootComponent(CollisionMesh);

	VisibleMeshSkeletal = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("MeshSkeletal"));
	VisibleMeshSkeletal->SetCollisionEnabled(ECollisionEnabled::Type::NoCollision);
	VisibleMeshSkeletal->AttachTo(GetRootComponent());

	VisibleMeshStatic = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshStatic"));
	VisibleMeshStatic->SetCollisionEnabled(ECollisionEnabled::Type::NoCollision);
	VisibleMeshStatic->AttachTo(GetRootComponent());

	SetActorEnableCollision(true);
	
	OnActorBeginOverlap.AddDynamic(this, &ABasePickup::OnCollision);
}

// Called when the game starts or when spawned
void ABasePickup::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABasePickup::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
}

void ABasePickup::OnCollision( AActor * OtherActor)
{
	AGameplayCharacter * tmp = Cast<AGameplayCharacter>(OtherActor);

	if (tmp) HitGameplayCharacter(tmp);
}

void ABasePickup::PlayPickupSound()
{
	if (PickupSound)
	{
		//UGameplayStatics::PlaySoundAttached(PickupSound, GetRootComponent());
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), PickupSound, GetActorLocation());
	}
}