// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "GameFramework/Controller.h"
#include "GameFramework/PlayerController.h"
#include "CurrentMaxTuple.h"
#include "BaseWeapon.generated.h"

UENUM(BlueprintType, Category = Enum)
namespace EGunStateEnum
{
	enum Type
	{
		GSE_Idle,
		GSE_Firing,
		GSE_Reloading
	};
}

UCLASS()
class CIS4900_API ABaseWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseWeapon();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	inline FCurrentMaxStruct GetMagAmmo(){ return FCurrentMaxStruct(CurrentAmmoInMag, MaxAmmoInMag); }
	inline FCurrentMaxStruct GetInventoryAmmo(){ return FCurrentMaxStruct(CurrentAmmoInInventory, MaxAmmoInInventory); }

	inline void SetInventoryAmmo(int32 NewAmmo){ CurrentAmmoInInventory = NewAmmo; }

	inline uint8 GetSlotNumber(){ return SlotNumber; }
	inline FName GetFPSocket(){ return AttachSocketName_FPP; }
	inline FName GetTPSocket(){ return AttachSocketName_TPP; }
	inline EGunEnum::Type GetEnum(){ return WeaponType.GetValue(); }
	
	inline bool GetInfiniteInventoryAmmo(){ return bInfiniteInventoryAmmo; }
	inline bool GetInfiniteMagAmmo(){ return bInfiniteMagAmmo; }
	inline void SetInfiniteInventoryAmmo(bool NewBool){ bInfiniteInventoryAmmo = NewBool; }
	inline void SetInfiniteMagAmmo(bool NewBool){ bInfiniteMagAmmo = NewBool; }

	UFUNCTION(BlueprintCallable, Category = Getter)
	EGunStateEnum::Type GetCurrentState();

	UFUNCTION(BlueprintCallable, Category = Enum)
	static FString EGunEnumToString(EGunEnum::Type EnumValue);

	void GiveAmmo(int32 AmmoToGive);

	ECollisionChannel GetWeaponTraceChannel();

	UFUNCTION(BlueprintCallable, Category = Functionality)
	virtual void StartFire();

	UFUNCTION(BlueprintCallable, Category = Functionality)
	virtual void StopFire();

	virtual bool Reload();

	virtual void OnSwitched();

	UPROPERTY(EditDefaultsOnly, Category=Sound)
	class USoundCue* EmptySound;

	UPROPERTY(EditDefaultsOnly, Category = FX) 
	class UParticleSystem* HitParticles;

	UPROPERTY(EditDefaultsOnly, Category = "Functionality")
		int32 AmmoOnPickup;

protected:

	FCollisionQueryParams TraceParams;

	virtual void SuccesfulFire();
	virtual void FailedFire();
	virtual void DealDamageTo(AActor* Target);

	void Fire();
	void PlaySound(USoundCue*SoundToPlay);
	void SpawnEmitterAtLocation(const FHitResult& Hit);
	void EndAnimation(UAnimMontage* Montage, bool bInterrupted);

	bool GetTraceStart(FVector& Loc, FRotator& Rot);

	float LineTraceDistance;
	
	FOnMontageEnded EndDelegate;

	UPROPERTY(VisibleDefaultsOnly, Category = Mesh) class USceneComponent * Root;
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)  class USkeletalMeshComponent * WeaponMesh;
	UPROPERTY(EditDefaultsOnly, Category = Animation) UAnimMontage* FireMontage;
	UPROPERTY(EditDefaultsOnly, Category = Animation) UAnimMontage* ReloadMontage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Functionality) FName AttachSocketName_FPP;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Functionality) FName AttachSocketName_TPP;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Functionality) FName AttachSocketName_Muzzle;

	TEnumAsByte<EGunEnum::Type> WeaponType;
	uint8 SlotNumber;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Functionality) TEnumAsByte<EGunStateEnum::Type> CurrentState;

	int32 CurrentAmmoInMag;
	
	int32 CurrentAmmoInInventory;

	UPROPERTY(EditDefaultsOnly, Category = "Functionality|Shooting")
		int32 MaxAmmoInMag;

	UPROPERTY(EditDefaultsOnly, Category = "Functionality|Shooting")
		int32 MaxAmmoInInventory;

	/*If the animations rate should be changed to match the specified FireRate.*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Functionality|Shooting")
		bool bCustomFireRate;
	/*Custom fire rate, will change animation rate to make it the speed specified here. (in seconds)s*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Functionality|Shooting", meta = (EditCondition = "bCustomFireRate", ClampMin = "0.0"))
		float FireRate;
	
	/*If the weapon should continue firing after the first shot. If it is false, StartFire() has to be called for each shot.*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Functionality|Shooting")
		bool bAutomatic;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Functionality|Shooting")
		float DamagePerShot;
	
	UPROPERTY(EditAnywhere, Category = "Functionality|Shooting") 
		bool bInfiniteMagAmmo;
	UPROPERTY(EditAnywhere, Category = "Functionality|Shooting") 
		bool bInfiniteInventoryAmmo;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Functionality|Shooting")
		bool bCustomReloadTime;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Functionality|Shooting", meta=(EditCondition="bCustomReloadTime", ClampMin="0.0")) 
		float ReloadTime;

	/*The ange of the cone when shooting*/
	UPROPERTY(EditAnywhere, Category = "Functionality|Shooting")
		int32 InaccuracyAngle;

	bool bWantsToFire;

	class AGameplayCharacter* OwningCharacter;

	FTimerHandle FireHandle;
	FTimerHandle FireAnimationHandle;
	FTimerHandle ReloadAnimationHandle;

protected:
	inline bool HaveAmmo() { return (CurrentAmmoInInventory != 0 || bInfiniteInventoryAmmo); }
	inline bool IsMagNotFull(){ return (CurrentAmmoInMag != MaxAmmoInMag); }

	bool Trace(const FVector & Start, const FRotator & Rot, FHitResult& Result);

	bool CanDamage(const AActor* Target);

	float GetFireRate(float FireAnimTotal);
	float GetReloadRate(float ReloadAnimTotal);

};
