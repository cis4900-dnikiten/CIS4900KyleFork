// Fill out your copyright notice in the Description page of Project Settings.

#include "CIS4900.h"
#include "CurrentMaxTuple.h"
#include "BaseWeapon.h"
#include "Shotgun.h"
#include "ShooterGameInstance.h"
#include "PlayerStats.h"

UPlayerStats::UPlayerStats() : HPCurrent(PS_CURRENT_HEALTH_START), HPMax(PS_MAX_HEALTH_START), ArmorCurrent(PS_CURRENT_ARMOR_START), ArmorMax(PS_MAX_ARMOR_START), BatteryPowerCurrent(PS_MAX_BATTERY_POWER), BatteryPowerMax(PS_MAX_BATTERY_POWER),
bHasKey1(false), bHasKey2(false), bHasKey3(false), bWasInitialized(false)
{
	LowHPThreshold = 0.2;
}

void UPlayerStats::Load(UGameplaySaveGame* Save)
{
	HPCurrent = Save->CurrentHP;
	ArmorCurrent = Save->CurrentArmor;

	HasWeapon[2] = Save->bHasWeapon2;
	HasWeapon[3] = Save->bHasWeapon3;
	HasWeapon[4] = Save->bHasWeapon4;

	if (FindWeaponWithSlot(2)) FindWeaponWithSlot(2)->SetInventoryAmmo(Save->WeaponAmmo2);
	if (FindWeaponWithSlot(3)) FindWeaponWithSlot(3)->SetInventoryAmmo(Save->WeaponAmmo3);
	if (FindWeaponWithSlot(4)) FindWeaponWithSlot(4)->SetInventoryAmmo(Save->WeaponAmmo4);
}

void UPlayerStats::InitGunArray(UWorld* World,AActor* Owner, USceneComponent* FPAttachTo, USceneComponent* TPAttachTo)
{
	if (bWasInitialized || !FPAttachTo) return;

	UShooterGameInstance * CurrentInstance = nullptr;
	if(World) CurrentInstance = Cast<UShooterGameInstance>(World->GetGameInstance());

	FActorSpawnParameters SpawnParams;
	SpawnParams.Owner = Owner;
	if (CurrentInstance)
	{
		auto AllWeapons = CurrentInstance->GetAllPossibleWeaponClasses();

		for (auto i : AllWeapons)
		{
			ABaseWeapon* tmp = World->SpawnActor<ABaseWeapon>(i,SpawnParams);
			tmp->SetActorHiddenInGame(true);
			tmp->AttachRootComponentTo(FPAttachTo, tmp->GetFPSocket());

			WeaponInventory.Add(tmp);

			HasWeapon.Add(tmp->GetSlotNumber(), false);
		}
	}

	//HasWeapon.Add(1, true);
	//HasWeapon.Add(2, true);

	bWasInitialized = true;
}

uint8 UPlayerStats::GetSlot(EGunEnum::Type T)
{
	ABaseWeapon* Tmp = FindWeaponWithEnum(T);
	if (Tmp) return Tmp->GetSlotNumber();
	else return 1;
}

ABaseWeapon* UPlayerStats::MakeSlotCurrent(uint8 Slot)
{
	//if we dont have the weapon at that slot or its already equipt
	if (!HasWeapon[Slot]) return nullptr;
	if (CurrentWeapon && CurrentWeapon->GetSlotNumber() == Slot) return nullptr;

	ABaseWeapon* ToEquip = FindWeaponWithSlot(Slot);
	if (ToEquip)
	{
		if(CurrentWeapon) CurrentWeapon->OnSwitched();
		CurrentWeapon = ToEquip;
	}
	
	return ToEquip;
}

FCurrentMaxStruct UPlayerStats::GetGunMagAmmoByEnum(EGunEnum::Type WhatGun)
{
	ABaseWeapon* tmp = FindWeaponWithEnum(WhatGun);
	if (tmp) return tmp->GetMagAmmo();
	return FCurrentMaxStruct(0, 0);
}
FCurrentMaxStruct UPlayerStats::GetGunMagAmmoBySlot(uint8 WhatGun)
{ 
	ABaseWeapon* tmp = FindWeaponWithSlot(WhatGun);
	if (tmp) return tmp->GetMagAmmo();
	return FCurrentMaxStruct(0, 0); 
}
FCurrentMaxStruct UPlayerStats::GetCurrentGunMagAmmo()
{
	if (CurrentWeapon) return CurrentWeapon->GetMagAmmo();
	return FCurrentMaxStruct(0, 0); 
}
FCurrentMaxStruct UPlayerStats::GetGunInventoryAmmoByEnum(EGunEnum::Type WhatGun)
{
	ABaseWeapon* tmp = FindWeaponWithEnum(WhatGun);
	if (tmp) return tmp->GetInventoryAmmo();
	return FCurrentMaxStruct(0, 0);
}
FCurrentMaxStruct UPlayerStats::GetGunInventoryAmmoBySlot(uint8 WhatGun)
{
	ABaseWeapon* tmp = FindWeaponWithSlot(WhatGun);
	if (tmp) return tmp->GetInventoryAmmo();
	return FCurrentMaxStruct(0, 0);
}
FCurrentMaxStruct UPlayerStats::GetCurrentGunInventoryAmmo()
{
	if (CurrentWeapon) return CurrentWeapon->GetInventoryAmmo();
	return FCurrentMaxStruct(0, 0);
}
EGunEnum::Type UPlayerStats::GetCurrentGunEnum()
{
	if (CurrentWeapon) return CurrentWeapon->GetEnum();
	return EGunEnum::Type::GE_Knife;
}
int32 UPlayerStats::GetCurrentGunPickupAmmo()
{
	if (CurrentWeapon) return CurrentWeapon->AmmoOnPickup;
	return 0;
}

FCurrentMaxStruct UPlayerStats::GetHealth()
{
	return FCurrentMaxStruct(HPCurrent, HPMax);
}

bool UPlayerStats::IsLowHealth()
{
	if (((float)HPCurrent / (float)HPMax) < LowHPThreshold)
	{
		return true;
	}
	return false;
}

//Health Setters
void UPlayerStats::SetHealthCurrentMax(FCurrentMaxStruct New)
{
	HPCurrent = New.Current;
	HPMax = New.Max;
}
void UPlayerStats::SetHealth(int32 New)
{
	HPCurrent = New;
}
void UPlayerStats::AddHealth(int32 ToAdd)
{
	HPCurrent = FMath::Clamp<int32>(HPCurrent + ToAdd, 0, HPMax);
}

//Armor Setters
void UPlayerStats::SetArmorCurrentMax(FCurrentMaxStruct New)
{
	ArmorCurrent = New.Current;
	ArmorMax = New.Max;
}
void UPlayerStats::SetArmor(int32 New)
{
	ArmorCurrent = New;
}
void UPlayerStats::AddArmor(int32 ToAdd)
{
	ArmorCurrent = FMath::Clamp<int32>(ArmorCurrent + ToAdd, 0, ArmorMax);
}

FCurrentMaxStruct UPlayerStats::GetArmor()
{
	return FCurrentMaxStruct(ArmorCurrent, ArmorMax);
}

FCurrentMaxStruct UPlayerStats::GetBatteryPower()
{
	return FCurrentMaxStruct(BatteryPowerCurrent, BatteryPowerMax);
}

void UPlayerStats::AddAmmoCurrent(int32 Amount)
{
	if (CurrentWeapon) CurrentWeapon->GiveAmmo(Amount);
}
void UPlayerStats::AddAmmoBySlot(int32 Amount, uint8 Slot)
{
	ABaseWeapon* tmp = FindWeaponWithSlot(Slot);
	if (tmp) tmp->GiveAmmo(Amount);
}
void UPlayerStats::AddAmmoByEnum(int32 Amount, EGunEnum::Type Gun)
{
	ABaseWeapon* tmp = FindWeaponWithEnum(Gun);
	if (tmp) tmp->GiveAmmo(Amount);
}

bool UPlayerStats::HasWeaponBySlot(uint8 Slot)
{
	return (HasWeapon.Contains(Slot) && HasWeapon[Slot]);
}
bool UPlayerStats::HasWeaponByEnum(EGunEnum::Type Gun)
{
	ABaseWeapon* tmp = FindWeaponWithEnum(Gun);
	return (tmp && HasWeapon.Contains(tmp->GetSlotNumber()) && HasWeapon[tmp->GetSlotNumber()]);
}

bool UPlayerStats::HasKey(EKeysEnum::Type Key)
{
	switch (Key)
	{
	case (EKeysEnum::Type::KE_RED):
			return bHasKey1;
	case (EKeysEnum::Type::KE_BLUE): 
			return bHasKey2;
	case (EKeysEnum::Type::KE_YELLOW):
			return bHasKey3;
	}

	return false;
}

void UPlayerStats::GiveWeaponBySlot(uint8 Slot)
{
	if (!HasWeapon.Contains(Slot)) return;

	ABaseWeapon* tmp = FindWeaponWithSlot(Slot);

	//if we dont have the gun, add it
	if (tmp && !HasWeapon[Slot])
	{	
		HasWeapon[Slot] = true;
	}
	//and give it some ammo either way
	tmp->GiveAmmo(tmp->AmmoOnPickup);
}
void UPlayerStats::GiveWeaponByEnum(EGunEnum::Type Gun)
{	
	ABaseWeapon* tmp = FindWeaponWithEnum(Gun);

	if (!tmp) return;

	uint8 Slot = tmp->GetSlotNumber();
	if (!HasWeapon.Contains(Slot)) return;
	
	//if we dont have the gun, add it
	if (!HasWeapon[Slot])
	{
		HasWeapon[Slot] = true;
	}
	//and give it some ammo either way
	tmp->GiveAmmo(tmp->AmmoOnPickup);
}

void UPlayerStats::GiveKey(EKeysEnum::Type Key)
{
	switch (Key)
	{
	case (EKeysEnum::Type::KE_RED) :
		bHasKey1 = true;
		return;
	case (EKeysEnum::Type::KE_BLUE) :
		bHasKey2 = true;
		return;
	case (EKeysEnum::Type::KE_YELLOW) :
		bHasKey3 = true;
		return;
	}
}

void UPlayerStats::AddBatteryPower(int32 Amount)
{
	BatteryPowerCurrent = FMath::Clamp(BatteryPowerCurrent + Amount, 0.0f, BatteryPowerMax);
}
bool UPlayerStats::SubBatteryPower(int32 Amount)
{
	BatteryPowerCurrent = FMath::Clamp(BatteryPowerCurrent - Amount, 0.0f, BatteryPowerMax);
	return (BatteryPowerCurrent == 0.0f);
}

uint8 UPlayerStats::GetNextSlot(uint8 CurrentSlot, uint8 Incrementer)
{
	uint8 TmpSlot = CurrentSlot + Incrementer;
	while (true)
	{
		if (TmpSlot == 0) TmpSlot = 10;
		if (TmpSlot == 11) TmpSlot = 1;

		if (HasWeaponBySlot(TmpSlot)) return TmpSlot;

		TmpSlot += Incrementer;
		if (TmpSlot == CurrentSlot) return CurrentSlot;
	}
}
//PRIVATE
ABaseWeapon* UPlayerStats::FindWeaponWithSlot(uint8 Slot)
{
	for (auto i : WeaponInventory)
	{
		if (i->GetSlotNumber() == Slot) return i;
	}
	return nullptr;
}
ABaseWeapon* UPlayerStats::FindWeaponWithEnum(EGunEnum::Type Enum)
{
	for (auto i : WeaponInventory)
	{
		if (i->GetEnum() == Enum) return i;
	}
	return nullptr;
}

