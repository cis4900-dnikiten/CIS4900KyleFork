// Fill out your copyright notice in the Description page of Project Settings.

#include "CIS4900.h"
#include "Pistol.h"


APistol::APistol()
{
	SlotNumber = 1;
	WeaponType = EGunEnum::Type::GE_Pistol;
	AttachSocketName_FPP = FName("S_Pistol");
	AttachSocketName_TPP = FName("Pistol");

	MaxAmmoInMag = 12;
	CurrentAmmoInMag = 12;
	MaxAmmoInInventory = -1;
	bInfiniteInventoryAmmo = true;
	bAutomatic = false;
}

void APistol::SuccesfulFire()
{
	Super::SuccesfulFire();

	FVector StartLoc;
	FRotator StartRot;
	if (!GetTraceStart(StartLoc, StartRot)) return;

	FHitResult Hit(ForceInit);
	Trace(StartLoc, StartRot, Hit);

	if (Hit.bBlockingHit)
	{
		DealDamageTo(Hit.GetActor());
	}

	SpawnEmitterAtLocation(Hit);
}



