// Fill out your copyright notice in the Description page of Project Settings.

#include "CIS4900.h"
#include "BaseWeapon.h"
#include "GameplayCharacter.h"
#include "ShooterGameInstance.h"

UShooterGameInstance::UShooterGameInstance()
{
	//AllWeaponClasses.SetNum(UShooterGameInstance::NumberOfWeapons);
	//Add shotgun
	AddBPClassToWeapons(TEXT("Blueprint'/Game/Roberts/Guns/BP_Shotgun.BP_Shotgun'"));
	//Add Assault rifle
	AddBPClassToWeapons(TEXT("Blueprint'/Game/Roberts/Guns/BP_AssRifle.BP_AssRifle'"));
	//Add Pistol
	AddBPClassToWeapons(TEXT("Blueprint'/Game/Roberts/Guns/BP_Pistol.BP_Pistol'"));
	//Add Railgun
	AddBPClassToWeapons(TEXT("Blueprint'/Game/Roberts/Guns/BP_RailGun.BP_RailGun'"));

	//CONSOLE DELEGATE SHIT
	CFuncSave.BindUObject(this, &UShooterGameInstance::ConsoleForceSave);
	IConsoleManager::Get().RegisterConsoleCommand(TEXT("ForceSave"), TEXT("ForceSave") TEXT("Forces a save of the current character"), CFuncSave, 0);
	CFuncLoad.BindUObject(this, &UShooterGameInstance::ConsoleForceLoad);
	IConsoleManager::Get().RegisterConsoleCommand(TEXT("ForceLoad"), TEXT("ForceLoad") TEXT("Forces intance to load and update character"), CFuncLoad, 0);
	CFuncDeleteSaves.BindUObject(this, &UShooterGameInstance::ConsoleDeleteSaves);
	IConsoleManager::Get().RegisterConsoleCommand(TEXT("DeleteAllSaves"), TEXT("DeleteAllSaves") TEXT("Deletes all saves WARNING NO RECOVERING"), CFuncDeleteSaves, 0);
	//END
}

FName UShooterGameInstance::GetSlotCurrentLevel(FString SlotName, int32 Index)
{
	if (UGameplayStatics::DoesSaveGameExist(SlotName, Index))
	{
		UGameplaySaveGame* Save = Cast<UGameplaySaveGame>(UGameplayStatics::LoadGameFromSlot(SlotName, Index));
		if (Save)
		{
			return Save->CurrentLevel;
		}
	}

	return FName(TEXT(""));
}

/*used to specify what save slot will be used*/
void UShooterGameInstance::StartGame(FString SlotName, int32 UserIndex)
{
	Slot = SlotName;
	Index = UserIndex;

	/*Create a save for the first time*/
	if (!UGameplayStatics::DoesSaveGameExist(Slot, Index))
	{
		UGameplaySaveGame* GameSlot = Cast<UGameplaySaveGame>(UGameplayStatics::CreateSaveGameObject(UGameplaySaveGame::StaticClass()));
		AGameplayCharacter::SaveDefault(GameSlot);
		GameSlot->CurrentLevel = FName(TEXT("Level1"));
		UGameplayStatics::SaveGameToSlot(GameSlot, Slot, Index);
	}
}

void UShooterGameInstance::ChangeLevel(FName NewLevel, bool Save, FName CurrentLevel)
{
	LoadingScreenLevelToLoad = NewLevel;

	if (Save)
	{
		SaveGame(CurrentLevel);
	}

	LoadingScreen();
}
/*Called to tell instance a level change was a success*/
void UShooterGameInstance::OnLevelFinishedLoading(bool Load)
{
	if (Load)
	{
		LoadGame();
	}
}

void UShooterGameInstance::DeleteSave(FString Slot, int32 Index)
{
	if(UGameplayStatics::DoesSaveGameExist(Slot,Index)) UGameplayStatics::DeleteGameInSlot(Slot, Index);
}

AGameplayCharacter* UShooterGameInstance::FindPlayer()
{
	return Cast<AGameplayCharacter>(GetWorld()->GetFirstPlayerController()->GetCharacter());
}

void UShooterGameInstance::SaveGame(FName Level)
{

	UGameplaySaveGame* GameSlot = Cast<UGameplaySaveGame>(UGameplayStatics::CreateSaveGameObject(UGameplaySaveGame::StaticClass()));

	AGameplayCharacter* Player = FindPlayer();
	if (Player)
	{
		Player->Save(GameSlot);
	}

	if(Level == NAME_None) GameSlot->CurrentLevel = LoadingScreenLevelToLoad;
	else GameSlot->CurrentLevel = Level;
	UGameplayStatics::SaveGameToSlot(GameSlot, Slot, Index);
	
}
void UShooterGameInstance::LoadGame()
{
	UGameplaySaveGame* GameSlot = Cast<UGameplaySaveGame>(UGameplayStatics::LoadGameFromSlot(Slot, Index));

	AGameplayCharacter* Player = FindPlayer();
	if (Player && GameSlot)
	{
		Player->Load(GameSlot);
	}
}

void UShooterGameInstance::AddBPClassToWeapons(const wchar_t* Path)
{
	ConstructorHelpers::FObjectFinder<UBlueprint> WeaponToAdd( Path );
	if (WeaponToAdd.Object) 
	{
		AllWeaponClasses.Add ((UClass*)WeaponToAdd.Object->GeneratedClass);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Could not add blueprint at path %s"), Path);
	}
}

const TArray<TSubclassOf<ABaseWeapon>> UShooterGameInstance::GetAllPossibleWeaponClasses()
{
	return AllWeaponClasses;
}

//CONSOLE DELEGATE
void UShooterGameInstance::ConsoleForceSave()
{
	SaveGame();
}
void UShooterGameInstance::ConsoleForceLoad()
{
	LoadGame();
}
void UShooterGameInstance::ConsoleDeleteSaves()
{
	DeleteSave(UCIS4900Statics::GetSlot1Name(), 1);
	DeleteSave(UCIS4900Statics::GetSlot2Name(), 1);
	DeleteSave(UCIS4900Statics::GetSlot3Name(), 1);
}