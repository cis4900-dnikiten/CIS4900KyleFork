// Fill out your copyright notice in the Description page of Project Settings.

#include "CIS4900.h"
#include "GameplayCharacter.h"
#include "Bullet.h"
#include "AssRifle.h"

AAssRifle::AAssRifle()
{
	SlotNumber = 2;
	WeaponType = EGunEnum::Type::GE_AssRifle;
	AttachSocketName_FPP = FName("S_AssRifle");
	AttachSocketName_TPP = FName("AssRifle");

	MaxAmmoInMag = 100;
	MaxAmmoInInventory = 1000;

	bCustomFireRate = true;
	FireRate = 0.1f;
}

void AAssRifle::SuccesfulFire()
{
	Super::SuccesfulFire();

	FVector StartLoc;
	FRotator StartRot;

	FVector BulletStartLoc = WeaponMesh->GetSocketLocation(AttachSocketName_Muzzle);
	FRotator BulletStartRot;

	if (!GetTraceStart(StartLoc, StartRot)) return;

	FHitResult Hit(ForceInit);
	Trace(StartLoc, StartRot, Hit);

	if (Hit.bBlockingHit)
	{
		DealDamageTo(Hit.GetActor());
		SpawnEmitterAtLocation(Hit);
	}

	BulletStartRot = (Hit.Location - BulletStartLoc).Rotation();

	if (*TracerRound)
	{
		ABullet* Bullet = GetWorld()->SpawnActor<ABullet>(*TracerRound, BulletStartLoc, BulletStartRot);
		if(Bullet) Bullet->SetProperLifeSpan(LineTraceDistance);
	}
}

