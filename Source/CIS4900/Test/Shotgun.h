// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Test/BaseWeapon.h"
#include "Shotgun.generated.h"

/**
 * 
 */
UCLASS()
class CIS4900_API AShotgun : public ABaseWeapon
{
	GENERATED_BODY()

public:
	AShotgun();
	
	virtual void SuccesfulFire() override;

protected:
	UPROPERTY(EditDefaultsOnly, Category = Functionality)
		int32 ShotAngle;
	UPROPERTY(EditDefaultsOnly, Category = Functionality)
		uint8 PelletCount;
	
};
