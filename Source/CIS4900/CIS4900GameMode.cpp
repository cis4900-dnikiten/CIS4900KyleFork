// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "CIS4900.h"
#include "CIS4900GameMode.h"
#include "CIS4900HUD.h"
#include "CIS4900Character.h"

ACIS4900GameMode::ACIS4900GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPerson/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ACIS4900HUD::StaticClass();
}
