// Fill out your copyright notice in the Description page of Project Settings.

#include "CIS4900.h"
#include "CIS4900Statics.h"

FString UCIS4900Statics::GetSlot1Name()
{
	return FString(TEXT("Slot1"));
}
FString UCIS4900Statics::GetSlot2Name()
{
	return FString(TEXT("Slot2"));
}
FString UCIS4900Statics::GetSlot3Name()
{
	return FString(TEXT("Slot3"));
}

TArray<FHitResult> UCIS4900Statics::ConeTraceByChannel(
UWorld* WorldReference, const FVector & StartLoc, const FRotator & StartRot, int32 ConeHeight, const uint8 & NumberOfRays, const int32 & ConeAngle, const ECollisionChannel & TraceChannel,
	const FCollisionQueryParams& Params, const FCollisionResponseParams& ResponseParam)
{
	TArray<FHitResult> Ret;

	for (uint8 i = 0; i < NumberOfRays; i++)
	{
		FVector NewPoint = FMath::VRandCone(StartRot.Vector(), FMath::DegreesToRadians(ConeAngle / 2));
		FVector NewEnd = StartLoc + NewPoint* ConeHeight;

		FHitResult NewHit(ForceInit);

		WorldReference->LineTraceSingleByChannel(NewHit, StartLoc, NewEnd, TraceChannel, Params, ResponseParam);
		Ret.Add(NewHit);
	}

	return Ret;
}


