// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "TestActor.generated.h"

UCLASS()
class CIS4900_API ATestActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATestActor();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	//causes an error of "Unrecognized type TWeakPtr"
	////NOTE: if the reflection macro is removed, this compiles correctly
	//UPROPERTY(EditAnywhere, Category = Func)
	//	TWeakPtr<AActor> tmp;

	////causes an error of "Unrecognized type TSharedPtr"
	////NOTE: if the reflection macro is removed, this compiles correctly
	//UPROPERTY(EditAnywhere, Category = Func)
	//	TSharedPtr<AActor> tmp2;

	////causes a long messy error
	////Shared pointers are not compatible with UObjects
	//TSharedPtr<AActor> tmp2;

	UPROPERTY(VisibleAnywhere, Category = Foo)
		USceneComponent* Root;
	UPROPERTY(VisibleAnywhere, Category = Foo)
		UCapsuleComponent* Capsule;

	UPROPERTY(VisibleAnywhere, Category = Foo)
		int32 AnInteger;
	UPROPERTY(EditAnywhere, Category = Foo)
		int32 AnotherInteger;
};
