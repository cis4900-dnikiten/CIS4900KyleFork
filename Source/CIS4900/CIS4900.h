// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#ifndef __CIS4900_H__
#define __CIS4900_H__

#include "Engine.h"
#include "CIS4900Statics.h"
#include "Test/GameplaySaveGame.h"
#include "SoundDefinitions.h"

#define WEAPON_TRACE_CHANNEL ECC_GameTraceChannel2
#define BADMAN_TRACE_CHANNEL ECC_GameTraceChannel4
#define USE_TRACE_CHANNEL ECC_GameTraceChannel5
#endif
