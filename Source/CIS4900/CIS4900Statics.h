// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "CIS4900Statics.generated.h"

UENUM(BlueprintType, Category = Enum)
namespace EKeysEnum
{
	enum Type
	{
		KE_RED,
		KE_BLUE,
		KE_YELLOW
	};
}

UENUM(BlueprintType, Category = Enum)		//"BlueprintType" is essential to include
namespace EGunEnum
{
	enum Type
	{
		GE_Knife 	UMETA(DisplayName = "Knife"),
		GE_Shotgun 	UMETA(DisplayName = "Shotgun"),
		GE_AssRifle	UMETA(DisplayName = "AssaultRifle"),
		GE_BFG		UMETA(DisplayName = "BFG"),
		GE_RailGun	UMETA(DisplayName = "RailGun"),
		GE_Pistol	UMETA(DisplayName = "Pistol")
	};
}

UENUM(BlueprintType, Category = Enum)		//"BlueprintType" is essential to include
namespace EFlashlightEnum
{
	enum Type
	{
		FE_ON 	UMETA(DisplayName = "On"),
		FE_OFF	UMETA(DisplayName = "Off")
	};
}

UENUM(BlueprintType, Category = Enum)
namespace ELavalordState
{
	enum Type
	{
		LS_Idle UMETA(DisplayName = "Idle"),
		LS_Attacking UMETA(DisplayName = "Attacking"),
		LS_Jumping UMETA(DisplayName = "Jumping"),
		LS_Charging UMETA(DisplayName = "Charging"),
		LS_GrabHammer UMETA(DisplayName = "Grab_Hammer"),
		LS_Dead UMETA(DisplayName = "Dead")
	};
}

/**
 * 
 */
UCLASS()
class CIS4900_API UCIS4900Statics : public UObject
{
	GENERATED_BODY()

public:
	static TArray<FHitResult> ConeTraceByChannel(class UWorld* WorldReference,const FVector & StartLoc, const FRotator & StartRot,int32 ConeHeight,const uint8 & NumberOfRays, const int32 & ConeAngle, const ECollisionChannel & TraceChannel,
		const FCollisionQueryParams& Params = FCollisionQueryParams::DefaultQueryParam, const FCollisionResponseParams& ResponseParam = FCollisionResponseParams::DefaultResponseParam);	

	UFUNCTION(BlueprintCallable, Category = Functionality)
		static FString GetSlot1Name();
	UFUNCTION(BlueprintCallable, Category = Functionality)
		static FString GetSlot2Name();
	UFUNCTION(BlueprintCallable, Category = Functionality)
		static FString GetSlot3Name();
};
